using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroide : MonoBehaviour
{

    public AsteroideTipo tipo = AsteroideTipo.GRANDE;

    /// <summary>
    /// Velocidad del asteroide
    /// </summary>
    public float velocidad;

    /// <summary>
    /// Velocidad de rotaci�n del asteroide
    /// </summary>
    public float velocidadRotacion;

    /// <summary>
    /// Prefab de la explosi�n
    /// </summary>
    public GameObject explosion;

    GameManager gameManager;
    AudioManager audioManager;
    GeneradorAsteroides generador;

    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        gameManager = FindObjectOfType<GameManager>();
        audioManager = FindObjectOfType<AudioManager>();
        generador = FindObjectOfType<GeneradorAsteroides>();

        Propulsarse();
        Rotar();
    }

    /// <summary>
    /// A�ade una fuerza aleatoria al asteroide
    /// </summary>
    void Propulsarse()
    {
        float x = Random.Range(-1F, 1F);
        float z = Random.Range(-1F, 1F);

        Vector3 direccion = new Vector3(x, 0, z);

        rb.AddForce(direccion*velocidad);
    }

    /// <summary>
    /// A�ade una rotaci�n aleatoria al asteroide
    /// </summary>
    void Rotar()
    {
        float x = Random.Range(-1F, 1F);
        float y = Random.Range(-1F, 1F);
        float z = Random.Range(-1F, 1F);

        Vector3 rotacion = new Vector3(x, y, z);

        rb.AddTorque(rotacion * velocidadRotacion);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Asteroide")
        {
            gameManager.SumarPuntos(tipo);
            Destruirse();    
        }
    }

    /// <summary>
    /// Destruye el asteroide, instanciando una explosi�n
    /// </summary>
    void Destruirse()
    {
        audioManager.Explosion();
        Instantiate(explosion, transform.position, Quaternion.identity);

        generador.AsteroideDestruido(tipo, transform.position);

        gameManager.ComprobarGameOver();

        Destroy(gameObject);
    }

    public void CambiarTipo(AsteroideTipo nuevoTipo)
    {
        tipo = nuevoTipo;
    }
}
