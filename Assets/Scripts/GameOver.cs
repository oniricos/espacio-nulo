using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public Text puntuacionMaximaUI;

    private void Start()
    {
        //PlayerPrefs.DeleteAll();

        int puntuacionActual = PlayerPrefs.GetInt("puntuacion", 0);
        int puntuacionMaxima = PlayerPrefs.GetInt("puntuacionMax", 0);

        if (puntuacionActual > puntuacionMaxima)
        {
            PlayerPrefs.SetInt("puntuacionMax", puntuacionActual);
            puntuacionMaxima = puntuacionActual;
        }

        puntuacionMaximaUI.text = puntuacionMaxima.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            PlayerPrefs.DeleteKey("nivel");
            PlayerPrefs.DeleteKey("puntuacion");

            SceneManager.LoadScene(1);
        }
    }
}
