using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nave : MonoBehaviour
{
    public float velocidad = 10;
    public float velocidad_giro = 10;

    public GameObject prefabDisparo;
    public GameObject prefabExplosion;
    public GameObject escudo;

    public AudioManager audioManager;

    public GameManager gameManager;

    Rigidbody rb;

    bool invulnerable = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Disparar();
    }

    private void FixedUpdate()
    {
        Propulsarse();
        Girar();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destruirse();
    }

    /// <summary>
    /// Hace que la nave se propulse con fuerzas
    /// </summary>
    void Propulsarse()
    {
        float input = Input.GetAxis("Vertical");

        if (input > 0) {
            audioManager.Propulsion();
            rb.AddRelativeForce(Vector3.forward * velocidad);
        }
        else
        {
            audioManager.DetenerPropulsion();
        }
    }

    /// <summary>
    /// Gira la nave con fuerzas
    /// </summary>
    void Girar()
    {
        float input = Input.GetAxis("Horizontal");
        rb.AddRelativeTorque(Vector3.up * velocidad_giro * input);
    }

    void Disparar()
    {
        bool espacioPulsado = Input.GetButtonDown("Jump");

        if (espacioPulsado)
        {
            audioManager.Disparar();
            GameObject instancia = Instantiate(prefabDisparo, transform.position, transform.rotation);
            instancia.transform.Translate(0, 0, 0.5F);
            instancia.transform.Rotate(90, 0, 0);
        }

    }

    /// <summary>
    /// Desactivamos la nave
    /// </summary>
    void Destruirse()
    {
        if (!invulnerable)
        {
            audioManager.Explosion();
            audioManager.DetenerPropulsion();
            Instantiate(prefabExplosion, transform.position, Quaternion.identity);
            gameManager.RestarVida();
            gameObject.SetActive(false);

            int vidasRestantes = gameManager.ObtenerVidas();
            if (vidasRestantes > 0)
            {
                Invoke("Reactivar", 1);
            }
        }

    }

    void Reactivar()
    {
        //GetComponent<Collider>().enabled = false;
        invulnerable = true;
        escudo.SetActive(true);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
        gameObject.SetActive(true);

        //GetComponent<ControlLimites>().ResetearTrails();

        ControlLimites limites = GetComponent<ControlLimites>();
        limites.ResetearTrails();

        Invoke("DesactivarEscudo",3);
    }

    void DesactivarEscudo()
    {
        //GetComponent<Collider>().enabled = true;
        invulnerable = false;
        escudo.SetActive(false);
    }

    void HiperEspacio() { }
}
